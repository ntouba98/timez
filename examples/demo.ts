import Timez from '../timez';

// const dateNow = new Timez('03/03/2021 05:01:20 GMT+1', 'DD/MM/YYYY HH:mm:ss GMTZZ');
// dateNow.timezone = 3;

const dateNow = new Timez('Mon, 26 Dec 2022 23:36:58 GMT');

console.log(`> hivi.timez | dateNow.isValid():: `, dateNow.isValid());
console.log(`> hivi.timez | dateNow.toString():: `, dateNow.toString());
console.log(`> hivi.timez | dateNow.toDate():: `, dateNow.toDate());
console.log(`> hivi.timez | dateNow.timezone:: `, dateNow.timezone);
console.log(`> hivi.timez | dateNow.lang:: `, dateNow.lang);