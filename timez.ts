import moment from "moment";

export class Timez {
   private _date: moment.Moment = this.getCurrentDate();
   private _timezone: number = 0;
   private _lang: 'fr' | 'en' = 'fr';
   private _defaultFormat: string = 'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]'; // YYYY-MM-DD HH:mm:ss.SSS [GMT]ZZ
   private _format: string | undefined = this._defaultFormat;

    constructor(
        date: Date | string | undefined = undefined,
        format: string | undefined = undefined,
        lang: 'fr' | 'en' | undefined = undefined,
    ) {
        this.format = format;
        this.date = moment(date, this.format, true);
        this.lang = lang;

        // console.log('--> Timez - this.format:: ', this.format);
    }

    private getCurrentDate(): moment.Moment {
        return moment(undefined, true);
    }
    private getCurrentTimezone(): number {
        return parseFloat(
            String(this.getCurrentDate().toDate().getTimezoneOffset() / 60)
        ) * -1;
    }

    public parse(
        date: Date | string | undefined = undefined,
        format: string | undefined = undefined,
        lang: 'fr' | 'en' | undefined = undefined,
    ): this {
        this.format = format;
        this.date = date;
        this.lang = lang;
        return this;
    }
    public isValid() {
        return !!this._date.isValid();
    }

    public toString(
        format: string | undefined = undefined,
    ): string | undefined {
        this.format = format;
        return !!this.date.isValid() ? this.date.format(this.format) : undefined;
    }
    public toDate() {
        return !!this.date.isValid() ? this.date.toDate() : undefined;
    }
    public set date(date: Date | string | undefined | moment.Moment) {
        if(date instanceof moment) {
            this._date = date as any;
        } else if(typeof this._date === 'string') {

        } else {
            this._date = moment(date, this.format, true);
        }
    }
    public get date(): moment.Moment {
        return this._date;
    }
    public set timezone(timezone: number | undefined) {
        this._timezone = (!!timezone) ? timezone : this.getCurrentTimezone();
        this.date.utcOffset(this._timezone);
    }
    public get timezone(): number {
        return this._timezone;
    }
    public set lang(lang: 'fr' | 'en' | undefined) {
        this._lang = (!!lang && ['fr', 'en'].includes(lang)) ? lang : 'fr';
    }
    public get lang(): 'fr' | 'en' {
        return this._lang; 
    }
    public set format(format: string | undefined) {
        this._format = (!!format) ? format : undefined;
    }
    public get format(): string | undefined {
        return this._format;
    }
}

export default Timez;