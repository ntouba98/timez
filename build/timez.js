"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Timez = void 0;
const moment_1 = __importDefault(require("moment"));
class Timez {
    constructor(date = undefined, format = undefined, lang = undefined) {
        this._date = this.getCurrentDate();
        this._timezone = 0;
        this._lang = 'fr';
        this._defaultFormat = 'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]'; // YYYY-MM-DD HH:mm:ss.SSS [GMT]ZZ
        this._format = this._defaultFormat;
        this.format = format;
        this.date = (0, moment_1.default)(date, this.format, true);
        this.lang = lang;
        // console.log('--> Timez - this.format:: ', this.format);
    }
    getCurrentDate() {
        return (0, moment_1.default)(undefined, true);
    }
    getCurrentTimezone() {
        return parseFloat(String(this.getCurrentDate().toDate().getTimezoneOffset() / 60)) * -1;
    }
    parse(date = undefined, format = undefined, lang = undefined) {
        this.format = format;
        this.date = date;
        this.lang = lang;
        return this;
    }
    isValid() {
        return !!this._date.isValid();
    }
    toString(format = undefined) {
        this.format = format;
        return !!this.date.isValid() ? this.date.format(this.format) : undefined;
    }
    toDate() {
        return !!this.date.isValid() ? this.date.toDate() : undefined;
    }
    set date(date) {
        if (date instanceof moment_1.default) {
            this._date = date;
        }
        else if (typeof this._date === 'string') {
        }
        else {
            this._date = (0, moment_1.default)(date, this.format, true);
        }
    }
    get date() {
        return this._date;
    }
    set timezone(timezone) {
        this._timezone = (!!timezone) ? timezone : this.getCurrentTimezone();
        this.date.utcOffset(this._timezone);
    }
    get timezone() {
        return this._timezone;
    }
    set lang(lang) {
        this._lang = (!!lang && ['fr', 'en'].includes(lang)) ? lang : 'fr';
    }
    get lang() {
        return this._lang;
    }
    set format(format) {
        this._format = (!!format) ? format : undefined;
    }
    get format() {
        return this._format;
    }
}
exports.Timez = Timez;
exports.default = Timez;
//# sourceMappingURL=timez.js.map