import moment from "moment";
export declare class Timez {
    private _date;
    private _timezone;
    private _lang;
    private _defaultFormat;
    private _format;
    constructor(date?: Date | string | undefined, format?: string | undefined, lang?: 'fr' | 'en' | undefined);
    private getCurrentDate;
    private getCurrentTimezone;
    parse(date?: Date | string | undefined, format?: string | undefined, lang?: 'fr' | 'en' | undefined): this;
    isValid(): boolean;
    toString(format?: string | undefined): string | undefined;
    toDate(): Date | undefined;
    set date(date: Date | string | undefined | moment.Moment);
    get date(): moment.Moment;
    set timezone(timezone: number | undefined);
    get timezone(): number;
    set lang(lang: 'fr' | 'en' | undefined);
    get lang(): 'fr' | 'en';
    set format(format: string | undefined);
    get format(): string | undefined;
}
export default Timez;
//# sourceMappingURL=timez.d.ts.map