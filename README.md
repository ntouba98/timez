# @hivi/timez

## Presentation

**@hivi/timez** makes it easier to manipulate date type data.

## Installation

```console
npm i @hivi/timez -s
```

## Use

```js
import Timez from '../build/timez';

const dateNow = new Timez('03/03/2021 05:01:20 GMT+1', 'DD/MM/YYYY HH:mm:ss GMTZZ');
dateNow.timezone = 3;
console.log(`> hivi.timez | dateNow.isValid():: `, dateNow.isValid());
console.log(`> hivi.timez | dateNow.toString():: `, dateNow.toString());
console.log(`> hivi.timez | dateNow.toDate():: `, dateNow.toDate());
console.log(`> hivi.timez | dateNow.timezone:: `, dateNow.timezone);
console.log(`> hivi.timez | dateNow.lang:: `, dateNow.lang);
```

## Useful links

* [Git](/ "https://ntouba98@bitbucket.org/ntouba98/timez.git")
* [Nodejs dependency](/ "https://www.npmjs.com/package/@hivi/timez")